# Setting Model training on Alibaba Cloud

## Introduction
This document provides step by step setup of model training on Alibaba Cloud. The
trained model will be optimized through Tensorflow -> tflite -> ONNX -> Optimized
ONNX format and the trained model will be stored in the OSS bucket of Alibaba cloud.

## Pre-Requisites
- Alibaba Cloud Service Account.
- Following services must be activated for the Alibaba Cloud account.
    - Object Storage Service (OSS)
    - Machine Learning Platform for AI
    - Container Registry
- x86 host machine with docker installed.

<<<<<<< Updated upstream
## Setup Guide
Clone the project
Run the command given below to clone the project.
```sh
$ git clone -b <BRANCH_NAME> <PROJECT_GIT_URL>
```

Directory Structure for cloned project
- <PROJECT_ROOT>
    - config/
    - docs/
    - dataset_downloader/
    - helper/
    - model_data/
    - Dockerfile_AWS
    - Dockerfile_Alibaba
    - Dockerfile_Local_training
    - train_model.py
    - dataset_downloader.py
    - README_Dataset_downloading.md
    - README_AliCloud_Model_Training_setup.md
    - README_AWS_Model_Training_setup.md
    - README_Local_Model_Training_setup.md
=======
## Overview
- Configuring Resource Access Management user in Alibaba Cloud
- Creating the OSS bucket
- Build the docker image
- Creating a repository in Alibaba Container Registry
- Pushing the docker image to Container Registry
- Adding the image to Machine learning platform for AI on Alibaba cloud
- Creating and submitting a job
- View the training logs of the trained model
- What's next ?
>>>>>>> Stashed changes


## Configuring Resource Access Management user in Alibaba Cloud
A Resource Access Management (RAM) user is an entity created on Alibaba Cloud to allow end users to interact with Alibaba Cloud Services and authorize different access and permissions. Please Follow the below steps to set up the RAM.
- Open the Alibaba Cloud Page & Login with the Account Created.
- Search for **Resource Access Management** on Alibaba Cloud console. Access this service.
- On the left-side navigation pane, choose **Identities > Users**.
- On the **Users** page, click on the **Create User** button.
  - In the **User Account Information** section, enter the **Logon Name** and **Display Name**.
  - In the **Access Mode** section, select both of the access mode mentioned below:
    - **Console Access**. Select this option and complete the logon security settings.
      - Under **Set Logon Password**, select **Automatically Regenerate Default Password**.
      - Under **Password Reset**, select **Not Required**.
      - Under **Enable MFA** (multi-factor authentication), select **Not Required**.
    - **OpenAPI Access**
      - Upon selection, an AccessKey pair is automatically created for the RAM user.
      - The RAM user can call API operations or use other development tools to access Alibaba Cloud resources.

  - After configuring the Create User form, Click on **OK**.
    - Then, on the user created page, you can go to **User AccessKeys** section, click on the **Create AccessKey** button and download the CSV file.
- On the **Users** page, in the **Actions** column of the newly created user, click on **Add Permissions**.
    - Under **Select Policy**, search in the “Enter policy name” box and select:
        - AliyunOSSFullAccess
        - AliyunContainerRegistryFullAccess
        - AliyunPAIEASFullAccess
        - AliyunLogFullAccess
- Click on **OK** and then **Complete** button.

## Creating the OSS bucket
Object Storage Service (OSS) stores data as objects within buckets. Please Follow the below steps to setup the OSS.
- Search for **Object Storage Service** on Alibaba Cloud console. Access this service.
- In the left-side navigation pane, click on **Buckets** and then, click on **Create Bucket**.
- In the **Create Bucket** panel, configure the required parameters like the name of the bucket, and the region.
  - Select the preferred region for OSS bucket. In our case, we have used China (Shanghai) region.

    Let the remaining configuration to its default values.

  - Click on **OK**. This bucket is getting prepared for storing the compiled model by TVM.

## Build the docker image
- Navigate to the cloned project folder on machine, open the config file **cloud_config/alibaba_config.json** folder in an editor.
  - Update access_key, secret_key and region id (the region code for which region you are going to use the service; preffered China, Shanghai region).

  The access_key and secret_key will be present in the .csv file which was downloaded after the creation of the RAM user.
  - Update the OSS bucket name with the one created for saving the trained model.
  - Save the config file.
- Open a new terminal on the local machine and navigate to the root of the cloned project folder.
- Change the working directory to the root of the project folder
```sh
cd <PROJECT_ROOT>/
```
- Execute the below command to build the docker image.
  - The user needs to have a good internet connection as the image will download the dataset.
    ```sh
    sudo docker build -t <IMAGE_NAME>:<IMAGE_TAG> \
    -f Dockerfile_Alibaba .
    ```
      Users can specify the IMAGE_NAME & TAG_NAME of their choice. IMAGE_NAME is the name of the docker image. TAG_NAME is to tag the different variants of the same image.


## Creating a repository in Alibaba Container Registry
- Go to Alibaba Cloud console and search for **Container Registry** in the top
search bar. Access this service.
- Select the preferred region (recommended shanghai region for model training) at the top navigation bar.
- Click on **Instances** in the left side menu and click on **Instance of Personal Edition**.
- On **Instance of Personal Edition** page.
  - Click on **Repository > Namespace** in the left side menu. Click on **Create Namespace**.
    - Provide a name for the namespace of user choice and click on **Complete**.
  - Click on **Repository > Repositories** on the left side menu and click on **Create Repository**.
    - Select the namespace created previously from the dropdown list and provide a **Repository Name**. Keep the **Repository Type** as **Private** and provide a **Summary** for user reference. Click on **Next**.
  - Select **Local Repository** tab on top and click on **Create Repository**.

## Pushing the docker image to Container Registry
- After creating the repository, the Alibaba cloud will provide you with the steps for pushing an image.
- Open a terminal on the local device.
- Update the below command with user name as the email which was used to
create an Alibaba account and execute in the local terminal. Use the password of the Alibaba cloud for password to login.
```sh
sudo docker login --username=<EMAIL_ID> registry-intl.cn-shanghai.aliyuncs.com
```
- After the login succeeds, Update the below command and paste it in the local terminal and change the image name and tag.
    ```sh
    sudo docker tag <IMAGE_NAME>:<IMAGE_TAG> registry-intl.<REGION>.aliyuncs.com/<NAMESPACE>/<REPOSITORY_NAME>:<NEW_IMAGE_TAG>
    ```
    - <REGION>: Region id of the preferred region.
    - <IMAGE_NAME >:<IMAGE_TAG>: Image name and tag which were provided during building of docker image for training.
    - <NAMESPACE>: Namespace which was created previously.
    - <REPOSITORY_NAME>: Name of the repository which was created
    previously.
    - <NEW_IMAGE_TAG>: Provide a new image tag of user choice.
- Update the below command and execute on local terminal to push the image to **Container Registry**.
```sh
sudo docker push registry-intl.<REGION>.aliyuncs.com/<NAMESPACE>/<REPOSITORY_NAME>:<NEW_IMAGE_TAG>
```
- For more information on pushing and pulling images from the container registry, navigate to the **Repositories** page of the **Container Registry** console page, and click on the repository created previously. A guide will be provided by Alibaba Cloud.
- After the pushing is done, in the **Container Registry** cloud console, select **Tags** on the left side menu.
- We can see the pushed image with the tag provided.

## Adding the image to Machine learning platform for AI on Alibaba cloud
- Open the Alibaba Cloud console and search for **Machine learning Platform for AI**.
- If the workspace is not selected, select the default workspace created during the activation of the **Machine learning platform for AI** service. If selected, we can see the workspace on the top of the left side menu in the dropdown.
- On the left side menu, under **AI Computing Asset Management**, select **Images**.
- Select **Custom Images**, and click on **Add Image**.
  - Provide a custom image name.
  - In the **Associate image with container registry** section, provide the name in the below format.
  ```
  registry-intl.<REGION>.aliyuncs.com/<NAME_SPACE>/<REPO_NAME>:<IMAGE_TAG>
  ```
    - <REGION>: Region id in which the image has been pushed to the container registry.
    - <NAME_SPACE>: Namespace created during creation of the repository.
    - <REPO_NAME>: Repository name which was created previously.
    - <IMAGE_TAG>: Image tag given during the push of the image.
  - Under **CPU/GPU** select GPU and click on add image.

## Creating and submitting a job
- Go to **Machine learning Platform for AI** console.
- On the left side menu select **Jobs** and click on **Create Job**.
  - Provide the **Job name**, **Node image** as **customized** and in the dropdown, select the custom image which was created previously.
  - For the execution command, provide the following command.
    ```sh
    python3 train_model.py -m <MODEL_TYPE> -c <CLOUD_PLATFOEM> \
    -e <NUM_EPOCHS>
    ```
      Args:
      - -m: Type of model the user wants to train. Supported models are `yolov3/tiny_yolov3`.
      - -c: Cloud platform that the user is training the model on. Supported platforms are AWS/Alibaba.
      - -e: Number of epochs the model should be trained on. The preferred value would be 1500.
    - In the resources tab, select the **GPU** and select the instance in which the model is to be trained. The minimum configuration of the gpu instance is **4 CPU**, **30G disk space 1*Nvidia P100 (ecs.gn5-c4g1.xlarge)**.
    - Keep all the other options as default.
    - Submit the job

The job created may take a minimum of 30 minutes to start the execution of the training script.

## View the training logs of the trained model
- Go to **Machine learning Platform for AI** console and on left side menu, select **Jobs**.
- Select the job which was created and after the job has started, we can see the logs on the bottom side by selecting the logs option.
- After the model training has completed, we can see the trained model saved on OSS and under the bucket name created by the user.

## What's next ?
- Refer to the Model compilation repository README document.

