"""
Copyright © 2023 Arm Ltd and Contributors. All rights reserved.
SPDX-License-Identifier: Apache-2.0
"""
import numpy as np
import cv2

def preprocess_img(img, img_size, bond_boxes=None):
    
    img_h, img_w = img_size
    height, width, _ = img.shape

    img_scale = min(img_w / width, img_h / height)
    new_w, new_h = int(img_scale * width), int(img_scale * height)
    img_resized = cv2.resize(img, (new_w, new_h))

    img_paded = np.full(shape=[img_h, img_w, 3], dtype=np.uint8, fill_value=127)
    dw, dh = (img_w - new_w) // 2, (img_h - new_h) // 2
    img_paded[dh:new_h + dh, dw:new_w + dw, :] = img_resized

    if bond_boxes is None:
        return img_paded

    else:
        bond_boxes = np.asarray(bond_boxes).astype(np.float32)
        bond_boxes[:, [0, 2]] = bond_boxes[:, [0, 2]] * img_scale + dw
        bond_boxes[:, [1, 3]] = bond_boxes[:, [1, 3]] * img_scale + dh

        return img_paded, bond_boxes



def read_img(*args, **kwargs):
    return cv2.cvtColor(cv2.imread(*args, **kwargs), cv2.COLOR_BGR2RGB)


def postprocess_image(img, img_size, bond_boxes=None):
    
    img_h, img_w = img.shape[:2]
    height,width = img_size

    img_scale = min(img_w / width, img_h / height)
    new_w, new_h = int(img_scale * width), int(img_scale * height)
    dw, dh = (img_w - new_w) // 2, (img_h - new_h) // 2

    img = img[dh:new_h + dh, dw:new_w + dw, :]
    img_resized = cv2.resize(img, (width, height))

    if bond_boxes is None:
        return img_resized
    else:
        bond_boxes = bond_boxes.astype(np.float32)
        bond_boxes[:, [0, 2]] = np.clip((bond_boxes[:, [0, 2]] - dw) / img_scale, 0., width)
        bond_boxes[:, [1, 3]] = np.clip((bond_boxes[:, [1, 3]] - dh) / img_scale, 0., height)

        return img_resized, bond_boxes


